<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix'=>'posts'],function(){
    Route::get('/','codebrisk\demo\PostController@index');
    Route::get('/create','codebrisk\demo\PostController@create');
    Route::post('/store','codebrisk\demo\PostController@store');
    Route::get('/show/{id}','codebrisk\demo\PostController@show');
    Route::get('/edit/{id}','codebrisk\demo\PostController@edit');
    Route::post('/update/{id}','codebrisk\demo\PostController@update');
    Route::get('/delete/{id}','codebrisk\demo\PostController@destroy');
});
