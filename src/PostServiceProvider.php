<?php

namespace codebrisk\demo;

use Illuminate\Support\ServiceProvider;

class PostServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // Route
        include __DIR__.'/web.php';

        $this->loadViewsFrom(base_path('resources/views'),'posts');
        $this->publishes([
            __DIR__.'/views' => base_path('resources/views')
        ]);

        $this->publishes([
            __DIR__.'/migrations' => database_path('migrations')
        ],'migrations');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}