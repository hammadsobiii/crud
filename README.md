# CodeBrisk Demo Package

## Install

``` bash
"codebrisk/demo": "0.1.*"
```

## Usage

``` php
Insert into root directory composer.json file
"repositories":[
        {
            "type": "git",
            "url": "git@bitbucket.org:hammadsobiii/crud.git"
        }
    ],
'codebrisk\demo\PostServiceProvider::class'
php artisan vendor:publish
php artisan migrate
```